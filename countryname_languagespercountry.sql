SELECT country.name AS 'countryname', GROUP_CONCAT(DISTINCT language SEPARATOR ', ') AS 'Languages'
FROM countrylanguage
INNER JOIN country
ON country.code = countrylanguage.countrycode 
GROUP BY countryname;