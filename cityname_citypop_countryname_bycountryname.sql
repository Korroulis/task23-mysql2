SELECT city.name AS 'city name', city.population AS 'city population', country.name AS 'country name'
FROM city
INNER JOIN country
ON country.Code= city.CountryCode ORDER BY country.name;
